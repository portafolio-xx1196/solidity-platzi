// SPDX-License-Identifier: GPL

pragma solidity >=0.7 <0.9;

contract MyClass {
    struct Student {
        string name;
        string docId;
    }

    Student[] public students;

    constructor(){
        students.push(Student({
        name : "Andres",
        docId : "12345"
        }));
    }
}
