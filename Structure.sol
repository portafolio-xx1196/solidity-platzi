//add license recommended
// SPDX-License-Identifier: GPL

pragma solidity >=0.7 <0.9;//add versions

contract Structure {
    int quantity;
    uint quantityUnSigned;
    address myAddress;
    bool signature;

    constructor(bool isSigned){//constructor
        myAddress = msg.sender;
        signature = isSigned;
    }
}